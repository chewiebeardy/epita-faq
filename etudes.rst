Études à l'EPITA
================

Général
-------

Comment se déroule la scolarité à EPITA ?
    Les 5 ans d'études à l'école sont séparés en deux cycles :

    - Le cycle préparatoire, 2 ans, de BAC+1 à BAC+2. Comme dans les classes
      préparatoires "classiques", la première année s'appelle la SUP et la
      deuxième année la SPÉ. Pendant ces deux ans, l'accent est mis sur
      l'enseignement des sciences fondamentales (mathématiques, physique,
      électronique) tout en commençant l'apprentissage de l'informatique via
      des cours d'algorithmique et des TP de programmation.

    - Le cycle ingénieur, 3 ans, de BAC+3 à BAC+5. Les trois années du cycle
      ingénieur s'appellent respectivement ING1, ING2 et ING3. Pendant ces
      années, l'accent est mis sur l'apprentissage du métier d'ingénieur :
      informatique, mais également management, communication, droit, marketing,
      mais également mathématiques avancées et algorithmique.

Quelles sont les périodes de stage ?
    L'étudiant "normal" de l'EPITA réalisera pendant sa scolarité 3 stages :

    - Un stage à la fin du cycle préparatoire, de 2 à 3 mois, pendant les
      vacances d'été terminant l'année de SPÉ
    - Un deuxième stage, plus long (4 à 6 mois), après l'année d'ING1. Il peut
      commencer de Juillet à Septembre selon les cas, et se termine de Janvier
      à Février.
    - Le stage de fin d'études, commençant en février l'année de l'ING3 et
      durant lui 5 à 6 mois.

Où est disponible mon emploi du temps ?
    EPITA utilise l'application Chronos pour gérer les emplois du temps de
    toutes les classes et toutes les promotions. Vous pouvez accèder à Chronos
    à l'URL suivante : http://chronos.epita.net/

    Pour trouver votre emploi du temps sur Chronos, parcourez l'arbre à gauche
    de la page, en commençant par aller dans *Trainees* puis *EPITA*.

    Chronos étant assez peu pratique (vous le découvrirez bien vite...), des
    étudiants ont créés de nombreuses applications permettant de consulter son
    emploi du temps plus facilement. Vous pouvez par exemple utiliser
    l'application Epilife [1]_ sur votre téléphone Android.

.. [1] https://play.google.com/store/apps/details?id=com.iiie.epilife&hl=en

Quand puis-je faire mes études à l'étranger ?
    Lors du cursus, vous avez l'obligation d'effectuer au moins un semestre à
    l'étranger. Il existe plusieurs solutions à différents moments de votre
    scolarité.
    
    Le second semestre de SPÉ peut être effectué à l'étranger dans une des
    universités partenaires de l'EPITA. Parmi ces dernières, on peut
    généralement compter :

    - Stellenbosch University en Afrique du sud (http://www.sun.ac.za) 
    - Staffordshire University en Angleterre (http://www.staffs.ac.uk)
    - Griffith College Dublin en Irlande (http://www.gcd.ie)
    - Jiao Tong University en Chine (http://en.sjtu.edu.cn/)
    - D'autres en fonction de l'alignement des planètes

    Le semestre à l'étranger peut aussi s'effectuer lors d'un des stages du
    cycle ingénieur (il faut qu'il dure un minimum de temps pour valider
    l'étranger). Par exemple, avec un stage aux États-unis, c'est bon.

    La dernière possibilité, qui est plutôt une solution de secours si vous
    n'avez pas pu faire les deux précédentes, est de faire un semestre dans
    une des universités partenaires *après* le stage de fin d'étude (le stage
    de fin d'ING3).

    Il est évident que vous n'êtes pas restreint à un semestre à l'étranger. Par
    exemple vous pouvez faire le semestre de SPÉ en Irlande et faire vos stages
    aux États-unis. Un petit conseil cependant : si vous faites le semestre à
    l'étranger de SPÉ vous êtes libéré de cette contrainte. Par exemple vous
    envisagez peut-être de faire votre stage de fin d'étude, et là une boite en
    France vous propose un stage de rêve (si, sur un malentendu, c'est
    possible). Vous ne pourriez pas l'accepter car vous seriez obligé d'aller à
    l'étranger. Ce qui serait dommage.

Où se trouvent les locaux de l'administration de l'école ?
    TODO

Quelles sont les personnes de l'administration à contacter ?
    TODO

Qu'est-ce qu'une journée de TIG ?
    TODO

Cycle préparatoire (SUP/SPÉ)
----------------------------

.. todo::
    Cette section de la FAQ n'a pas encore été rédigée :-(

Cycle ingénieur (ING1/ING2/ING3)
--------------------------------

.. todo::
    Cette section de la FAQ n'a pas encore été rédigée :-(
